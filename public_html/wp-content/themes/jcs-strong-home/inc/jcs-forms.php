<?php
/* 
 * Project:    strong-home
 * File:       jcs-forms.php
 * Created:    Mar 22, 2022 10:28 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Handles front-end form processing.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */

function submit_donor_form() {
    if (isset($_POST['jcs_donors']) && $_POST['jcs_donors']) {
        $result = -10;
        $response = get_response($_POST['g-recaptcha-response']);
        if ( $response->success ) {
            $result = submit_contact_request();
            if ($result == 1) {
                $url = get_page_by_path('thank-you');
            } else {
                $url = get_page_by_path('form-error');
            }
        } else {
            $url = get_page_by_path('/');
        }

        wp_redirect( get_permalink($url) );
        exit();
    }

}
add_action('init', 'submit_donor_form');

function get_response($field) {
    // Proper handling of using recaptcha to block spam-bots from your form
    // 1. Get the recaptcha response from google before we do anything else
    $captcha = $field;
    // 2.  Set up the values to send to google for verification
    $fields = array(
        'secret' => (get_option('jcs_is_live') ) ? get_option('jcs_recaptcha_v3_secret_key') : get_option('jcs_recaptcha_v3_secret_key_staging'),
        'response' => $captcha,
        'remoteip' => $_SERVER['REMOTE_ADDR'] // User IP Address
    );
    // 3. User cURL to send the request
    $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
    // Configure the request
    curl_setopt($ch, CURLOPT_POST, true); // Google requires method: post
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  // We WANT a response from the server
    curl_setopt($ch, CURLOPT_TIMEOUT, 15); // 15 seconds then stop
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
    // execute the request
    $response = json_decode(curl_exec($ch));
    curl_close($ch);
    return $response;
}

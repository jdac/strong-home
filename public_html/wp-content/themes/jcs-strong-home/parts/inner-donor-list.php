<?php
    /* 
     * Project:    strong-home
     * File:       inner-donor-list.php
     * Created:    Mar 23, 2022 9:49 AM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description: Top-level template part for displaying state donors on the site donors page.
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    
    $states = get_states();
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-10 offset-md-1 donor-ty">
            <h2><span>Thank You to Our Project Sponsors</span></h2>
        </div>
    </div>
    <?php
        global $state;
        foreach( $states as $state_array):
            $state = $state_array['name'];
            get_template_part('parts/inner', 'state-donors');
        endforeach;
    ?>
</div>

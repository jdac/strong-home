<?php
/* 
 * Project:    strong-home
 * File:       Donors.php
 * Template Name: Donors
 * Created:    Mar 19, 2022 10:21 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template file for showing the site donors page.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
    get_header();
?>
    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

    <section id="program_links">
        <?php get_template_part('parts/content', 'programs'); ?>
    </section>

    <section id="hero_content">
        <?php
        while( have_posts() ): the_post();
            get_template_part('parts/content', 'strong-home');
        endwhile;
        ?>
    </section>

    <section id="donor_info">
        <?php get_template_part('parts/inner', 'donor-info'); ?>
    </section>

    <section id="donor_ty">
        <?php get_template_part('parts/inner', 'donor-list'); ?>
    </section>

    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

<?php
    get_footer();

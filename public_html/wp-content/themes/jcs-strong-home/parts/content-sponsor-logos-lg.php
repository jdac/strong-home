<?php
    /* 
     * Project:    strong-home
     * File:       ${NAME}
     * Created:    Mar 29, 2022 5:45 PM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description:
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    $lg_args = array(
        'post_type' => 'flash_sponsor',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key' => 'sponsor_class',
                'value' => 'national',
                'compare' => '='
            )
        )
    );
    $sponsors_lg = new WP_Query($lg_args);
    $count = $sponsors_lg->post_count;
    $mod_val = $count % 4;
    $the_posts = $sponsors_lg->posts;
?>
<div class="logo-spreader d-none d-lg-flex">
    <?php
        for ($i = 0; $i <= ($mod_val - 1); $i++) {
            $the_sponsor = $the_posts[$i];
            $the_post_id = $the_sponsor->ID;
            $the_link = get_field('link', $the_post_id);
            $img_uri = get_field('logo', $the_post_id);
            if ($img_uri) {
        ?>
            <div><a href="<?php echo $the_link; ?>"><img src="<?php echo $img_uri; ?>" alt="<?php echo get_the_title(); ?>" /></a></div>
        <?php
            }
        }
    ?>
    <div class="clear" style="flex-basis:100%;height:0;margin:0;"></div>
    <?php
        for ($i = $mod_val; $i < count($the_posts); $i++) {
            $the_sponsor = $the_posts[$i];
            $the_post_id = $the_sponsor->ID;
            $the_link = get_field('link', $the_post_id);
            $img_uri = get_field('logo', $the_post_id);
            if ($img_uri) {
                ?>
                <div><a href="<?php echo $the_link; ?>"><img src="<?php echo $img_uri; ?>" alt="<?php echo get_the_title(); ?>" /></a></div>
                <?php
            }
        }
    ?>
</div>

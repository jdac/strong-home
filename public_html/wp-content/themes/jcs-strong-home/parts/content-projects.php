<?php
/* 
 * Project:    strong-home
 * File:       content-projects.php
 * Created:    Mar 07, 2022 9:45 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: template part for showing projects/videos on site front page.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */

    global $post_id;

    // Gets the section heading content.
    $args = array(
        'name' => 'how-we-rebuild-better',
        'post_status' => 'publish',
        'posts_per_page' => 1,
    );
    $main_post = new WP_Query($args);

    // Project carousel content
    if ($post_id) {
        $proj_args = array(
            'post_type' => 'flash_project',
            'p' => $post_id,
            'posts_per_page' => 1
        );
    } else {
        $proj_args = array(
            'post_type' => 'flash_project',
            'post_status' => 'publish',
            'posts_per_page' => 1,
            'orderby' => 'menu_order',
            'order' => 'asc',
            'meta_query' => array(
                array(
                    'key' => 'show_on_front_page',
                    'value' => 'yes',
                    'compare' => '='
                )
            )
        );
    }
    $project = new WP_Query($proj_args);
    $current_project = false;
    $current_menu_order = false;

?>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-10 offset-md-1 rebuild">
            <?php while( $main_post->have_posts() ) : $main_post->the_post(); ?>
            <h2><?php echo get_the_title(); ?></h2>
            <p><?php echo get_the_content(); ?></p>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>
    <div class="row fp_projects_wrapper">
        <?php while ($project->have_posts()): $project->the_post(); ?>
            <div id="project_slider" class="royalSlider rsDefault col-12 col-md-8 offset-md-2">
                <!-- HTML content (100% with and height) -->
                <?php
                    $current_post = get_post(get_the_ID());
                    $folder_id = get_field('media_folder')[0];
                    $attachment_ids = get_attachment_ids_for_folder($folder_id);
                    foreach ($attachment_ids as $attachment_id) :
                        $img_url = wp_get_attachment_image_url($attachment_id, 'jcs-project-img');
                        $img_alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
                        $img_title = get_the_title($attachment_id);
                        $caption = wp_get_attachment_caption($attachment_id);
                        $description = get_post($attachment_id)->post_content;
                    ?>
                <div class="rsContent">
                    <img class="rsImg" src="<?php echo $img_url; ?>" alt="<?php echo $img_alt; ?>" />
                    <div class="seo-content">
                        <h3><?php echo $img_title; ?></h3>
                        <p><?php echo $caption; ?></p>
                        <p><?php echo $description; ?></p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <?php
                // Figure out ids for previous and next projects
                $current_menu_order = $current_post->menu_order;
                $current_project = get_the_ID();
            ?>
            <div class="project-desc col-12 col-md-8 offset-md-2">
                <h3><?php echo get_field('project_caption'); ?></h3>
                <?php echo get_the_content(); ?>
            </div>
        <?php endwhile; wp_reset_postdata(); ?>
        <div class="prjNav col-12 col-md-8 offset-md-2" id="project_nav">
            <?php
                // Get the IDs for the next and previous projects for the slide carousel
                $prev_id = false;
                $next_id = false;
                $all_proj_args = array(
                    'post_type' => 'flash_project',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'orderby' => 'menu_order',
                    'order' => 'asc',
                    'meta_query' => array(
                        array(
                            'key' => 'show_on_front_page',
                            'value' => 'yes',
                            'compare' => '='
                        )
                    )
                );
                $all_projects = new WP_Query($all_proj_args);
                $num_projects = $all_projects->found_posts;
                // Set menu orders of the posts we're looking for
                $prev_post_menu_order = ($current_menu_order == 0) ? -999 : $current_menu_order - 1;
                $next_post_menu_order = ($current_menu_order == ($num_projects - 1)) ? -999 : $current_menu_order + 1;
                
                while($all_projects->have_posts()): $all_projects->the_post();
                    $this_post = get_post(get_the_id());
                    $this_menu_order = $this_post->menu_order;
                    if ($this_menu_order == $prev_post_menu_order) {
                        $prev_id = get_the_ID();
                    }
                    if ($this_menu_order == $next_post_menu_order) {
                        $next_id = get_the_ID();
                    }
                endwhile; wp_reset_postdata();
            ?>
            <div class="prjArrow prjArrowLeft <?php echo (!$prev_id) ? 'prjArrowDisabled' : ''; ?>">
                <?php if ($prev_id) : ?>
                    <div class="prjArrowIcn"><a id="prev_arrow" class="ajax-req" href="<?php echo $prev_id; ?>"><span></span></a></div>
                <?php else : ?>
                    <div class="prjArrowIcn"><span></span></div>
                <?php endif; ?>
            </div>
            <div class="prjArrow prjArrowRight <?php echo (!$next_id) ? 'prjArrowDisabled' : ''; ?>">
                <?php if ($next_id) : ?>
                    <div class="prjArrowIcn"><a id="next_arrow" class="ajax-req" href="<?php echo $next_id; ?>"><span></span></a></div>
                <?php else : ?>
                    <div class="prjArrowIcn"><span></span></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php
/* 
 * Project:    strong-home
 * File:       content-strong-home.php
 * Created:    Feb 11, 2022 12:52 PM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering hero content on front-page
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
$page_id = get_the_ID();
$img_uri = get_the_post_thumbnail_url($page_id, 'jcs-hero');
?>

<div class="container">
    <div class="row">
        <div class="col-12 hero">
            <div id="hero" style="background-image: url(<?php echo $img_uri; ?>);"></div>
        </div>
        <div class="col-10 offset-1 content">
            <div class="strong-homes-content">
                <?php echo get_the_content(); ?>
            </div>
            <?php if (is_front_page()) : ?>
                <div class="recommend">
                    <?php get_template_part('parts/content', 'resilience-actions'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
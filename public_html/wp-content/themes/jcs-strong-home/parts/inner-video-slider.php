<?php
    /* 
     * Project:    strong-home
     * File:       inner-video-slider.php
     * Created:    Mar 26, 2022 12:56 PM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description: Template part for displaying to top video on the videos page.
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    $args = array(
        'post_type' => 'training_video',
        'posts_per_page' => 1,
        'meta_query' => array(
            array(
                'key' => 'show_at_top',
                'value' => 'yes',
                'compare' => '='
            )
        )
    );
    $video = new WP_Query($args);
    $num_videos = $video->post_count;
    
    $desc_args = array(
        'post_type' => 'post',
        'name' => 'training-video-intro-description',
        'post_status' => 'publish',
        'posts_per_page' => 1
    );
    $intro = new WP_Query($desc_args);
?>
<div class="container">
    <div class="row fp_video_wrapper">
        <div id="training_intro" class="royalSlider rsDefault col-12 col-md-10 offset-md-1">
            <?php while( $video->have_posts() ) : $video->the_post();
                $img_uri = get_the_post_thumbnail_url(get_the_ID(), 'medium_large');
                $thumb_uri = get_the_post_thumbnail_url(get_the_ID(), 'jcs-video-thumb');
                $vid_link = get_field('video_link');
                $caption = get_field('caption');
                ?>
                <div class="rsContent">
                    <img class="rsImg" src="<?php echo $img_uri; ?>" data-rsVideo="<?php echo $vid_link; ?>" />
                    <?php if ($caption != '') : ?>
                        <div class="rsCaption"><?php echo $caption; ?></div>
                    <?php endif; ?>
                    <div class="rsTmb">
                        <img src="<?php echo $thumb_uri; ?>" />
                        <div class="thumb-cap"><p><?php echo $caption; ?></p></div>
                    </div>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <?php while( $intro->have_posts() ): $intro->the_post(); ?>
            <div class="col-12 col-md-10 offset-md-1 training-info">
                <?php the_content(); ?>
            </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
</div>

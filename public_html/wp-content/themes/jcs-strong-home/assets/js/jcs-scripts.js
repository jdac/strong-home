/*
 * Project:    Strong-Homes
 * File:       jcs-scripts.js
 * Created:    Mar 29, 2021 2:42:23 PM
 * Updated:
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://jdacsolutions.com
 *
 * Description:  Provides main js functionality for Tornado Strong
 *               jcs-tornado-strong WP Theme
 *
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

$ = jQuery.noConflict();

$(document).ready(function($) {
    console.log("We're Loaded");
    let sliderHeight = 520;
    let sliderWidth = 950;
    let mq = window.matchMedia( "(min-width: 1280px)" );
    if (!mq.matches) {
        sliderHeight = 480;
    }
    $("#fp_videos").royalSlider({
        // options go here
        // as an example, enable keyboard arrows nav
        keyboardNavEnabled: true,
        autoScaleSlider: true,
        autoScaleSliderWidth: 4,
        autoScaleSliderHeight: 2.5,
        imageScaleMode: 'fill',
        //imageScalePadding: 0,
        autoPlay: {
            enabled: false
        },
        loop: false,
        arrowsNav: false,
        transitionType: 'fade',
        //controlNavigation: 'thumbnails',
        controlNavigation: 'none',
        thumbs: {
            spacing: 15,
            fitInViewport: false,
            autoCenter: true,
            arrowLeft: $('#fp_thumb_left'),
            arrowRight: $('#fp_thumb_right')
        },
        globalCaption: true
    });

    $("#project_slider").royalSlider({
        // options go here
        // as an example, enable keyboard arrows nav
        keyboardNavEnabled: true,
        autoScaleSlider: true,
        autoScaleSliderWidth: 4,
        autoScaleSliderHeight: 2.5,
        imageScaleMode: 'fill',
        //imageScalePadding: 0,
        autoPlay: {
            enabled: false
        },
        loop: false,
        arrowsNav: false,
        transitionType: 'fade',
        //controlNavigation: 'thumbnails',
        controlNavigation: 'bullets',
        globalCaption: false
    });

    $("#tweets_rs").royalSlider({
        // options go here
        // as an example, enable keyboard arrows nav
        keyboardNavEnabled: true,
        //autoScaleSlider: true,
        //autoScaleSliderWidth: 10,
        //autoScaleSliderHeight: 7.5,
        //imageScaleMode: 'fill',
        //imageScalePadding: 0,
        //slideSpacing: 2,
        autoPlay: {
            enabled: true,
            delay: 5000
        },
        loop: true,
        arrowsNav: false,
        //transitionType: 'fade',
        controlNavigation: 'none',
        globalCaption: false
    });

    //FluidBox Plug-in
    $('.gallery a').each(function() {
        $(this).attr({'data-fluidbox' : ''});
    });

    if($('[data-fluidbox]').length > 0) {
        $('[data-fluidbox]').fluidbox();
    }

    set_prj_nav_height();

});

// Set height of front-page project slider nav
function set_prj_nav_height() {
    let slider_height = $('#project_slider').height();
    console.log('Slider height: ' + slider_height);
    $('#project_nav').height(slider_height);
}

// Toggles for home schematic
$('.gs-toggle').on('click', function() {
    let target = '#' + $(this).attr('data-target');
    if ($(this).hasClass('open')) {
        $(this).removeClass('open');
        $(target).removeClass('show');
    } else {
        $('.gs-toggle').removeClass('open');
        $('.gold-standard').removeClass('show');
       $(this).addClass('open');
       $(target).addClass('show');
    }
});

// Copy stuff
$("[id^=link_copy_]").on('click', function() {
    let target = '#' + $(this).attr('id').substring(5); // all paragraph ids are copy_nnn matching link_copy_nnn
    let input = document.createElement("textarea");
    //adding p tag text to textarea
    input.value = $(target).text();
    document.body.appendChild(input);
    input.select();
    document.execCommand("Copy");
    // removing textarea after copy
    input.remove();
});

// Front page project swapping
$(document).on('click', '.ajax-req', function(e) {
    e.preventDefault();
    let target = $(this).attr('href');
    send_project_ajax(target);
});
function send_project_ajax(target) {
    $.ajax({
        type: 'post',
        data: {
            'action': 'jcs_swap_project',
            'id': target
        },
        url: admin_ajax.ajax_url,
        success: function (data) {
            $("#projects").html(data);
            $("#project_slider").royalSlider({
                // options go here
                // as an example, enable keyboard arrows nav
                keyboardNavEnabled: true,
                autoScaleSlider: true,
                autoScaleSliderWidth: 4,
                autoScaleSliderHeight: 2.5,
                imageScaleMode: 'fill',
                //imageScalePadding: 0,
                autoPlay: {
                    enabled: false
                },
                loop: false,
                arrowsNav: false,
                transitionType: 'fade',
                //controlNavigation: 'thumbnails',
                controlNavigation: 'bullets',
                globalCaption: false
            });
            set_prj_nav_height();
        },
        error: function() {
            alert("Post not found");
        }
    });
}

// Donor contact form submission
$('#donor_submit').on('click', function(event) {
    event.preventDefault();
    grecaptcha.ready(function() {
        grecaptcha.execute(f_site_key.value, {action: 'submit_donor_form'}).then(function(token) {
            $('#g-recaptcha-response').val(token);
            //console.log(token);
            $('#donor_inputs').submit();
        });
    });
});


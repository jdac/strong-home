<?php
    /*
     * Project:    FLASH WP NDRC
     * File:       functions.php
     * Created:    Dev 18, 2021 09:54
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     *
     * Description: Theme function file for the JCS NDRC Theme.
     *
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     *
     */
    
    global $pagenow;

    // Customizes the login screens
    function jcs_admin_styles() {
        wp_enqueue_style('loginCSS', get_template_directory_uri() . '/login/css/login-styles.css', array(), '1.0.0', 'all');
    }
    add_action('login_enqueue_scripts', 'jcs_admin_styles', 10);
    
    // Login screen changes
    function sh_login_logo_url() {
        return home_url();
    }
    add_filter( 'login_headerurl', 'sh_login_logo_url' );
    
    function sh_login_logo_url_title() {
        return 'Federal Alliance for Safe Homes Strong-Homes';
    }
    add_filter( 'login_headertext', 'sh_login_logo_url_title' );
    
    // Handles Theme options
    if (file_exists(get_template_directory() . '/inc/jcs-options.php')) {
        require_once(get_template_directory() . '/inc/jcs-options.php');
    }
    // Handles Theme Forms
    if (file_exists(get_template_directory() . '/inc/jcs-forms.php')) {
        require_once(get_template_directory() . '/inc/jcs-forms.php');
    }
    // JCS Widgets
    if (file_exists(get_template_directory() . '/inc/jcs-widgets.php')) {
        require_once(get_template_directory() . '/inc/jcs-widgets.php');
    }
    // Handles theme admin modifications; sortable custom post fields, etc.
    if (is_admin() && file_exists(get_template_directory() . '/inc/jcs-admin.php')) {
        require_once(get_template_directory() . '/inc/jcs-admin.php');
    }

    /*
     * Theme Support
     */
    function jcs_theme_support() {

        // Turn off admin bar on front end while logged in
        add_filter('show_admin_bar', '__return_false');

        // Features
        add_theme_support('title-tag');  // Auto handle page names in browser
        add_theme_support('post-thumbnails');  // Allow featured images

        // Localisation
        load_theme_textdomain('jcs-strong-home', get_template_directory() . '/languages');

        // Custom image sizes
        add_image_size('jcs-hero', 1145, 409, array('left','top'));  // True crops.  You can also specify hard crops telling where to crop.  See function reference.
        add_image_size('jcs-video-img', 630, 375, true);  // Front page video thumbnails
        add_image_size('jcs-video-thumb', 250,150,array('left', 'bottom')); // Front page disaster images
        add_image_size('jcs-project-img', 855, 472, true);  // Front page video thumbnails
        add_image_size('jcs-project-thumb', 250,150,array('left', 'bottom')); // Front page disaster images
        add_image_size('jcs-res-icon', 80, 80, true); // Resilience Recommendation icons
        add_image_size('jcs-donor-tile', 322, 242, true); // Donor info tiles
        add_image_size('jcs-training-thumb', 890, 495, true); // Training Video Thumbs
        // Resize the standard WP thumbnail
        update_option('thumbnail_size_w', 253);
        update_option('thumbnail_size_h', 164);
        update_option('thumbnail_crop', array('left', 'top'));
    }
    add_action('after_setup_theme', 'jcs_theme_support');

    // Make theme custom image sizes available for selection in the back end
    function jcs_custom_sizes( $sizes ): array {
        // Add a line to the array for each size you want available in admin
        return array_merge( $sizes, array(
            'jcs-hero' => __('Hero Image', 'jcs-strong-home'),
            'jcs-video-img' => __('Video Image', 'jcs-strong-home'),
            'jcs-video-thumb' => __('Video Thumbnail', 'jcs-strong-home'),
            'jcs-project-img' => __('Project Video Image', 'jcs-strong-home'),
            'jcs-project-thumb' => __('Project Video Thumbnail', 'jcs-strong-home'),
            'jcs-res-icon' => __('Resilience Icon', 'jcs-strong-home'),
      ));
    }
    add_filter('image_size_names_choose', 'jcs_custom_sizes');

    /*
     * Theme Styles
     */
    function jcs_enqueue_styles() {
        // Normalize
        wp_register_style('normalize', get_template_directory_uri() . '/assets/css/normalize.css', array(), '8.0.1', 'all');
        wp_enqueue_style('normalize');

        // Bootstrap 4.5.0
        wp_register_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css', array(), '4.5.0', 'all');
        wp_enqueue_style('bootstrap');

        // Google Fonts
        wp_register_style('Source-Sans-Pro', 'https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap', array(), '1.0', 'all');
        wp_enqueue_style('Source-Sans-Pro');

        // Font Awesome 5.15
        wp_register_style('fontawesome', get_template_directory_uri() . '/assets/css/all.min.css', array(), '5.15', 'all');
        wp_enqueue_style('fontawesome');

        // FluidBox
        wp_register_style('fluidbox', get_template_directory_uri() . '/assets/css/fluidbox.min.css', array(), '2.0.5');
        wp_enqueue_style('fluidbox');

        wp_register_style('royal-slider', get_template_directory_uri() . '/assets/royalslider/royalslider.css', array(), '1.05','all');
        wp_enqueue_style('royal-slider');
        wp_register_style('royal-slider-skin', get_template_directory_uri() . '/assets/royalslider/skins/default/rs-default.css', array(), '1.05','all');
        wp_enqueue_style('royal-slider-skin');

        // JCS Service Main Style
        wp_register_style('jcs-strong-home-style', get_template_directory_uri() . '/assets/css/style.css', array(), '1.0.0', 'all');
        wp_enqueue_style('jcs-strong-home-style');

    }
    add_action('wp_enqueue_scripts', 'jcs_enqueue_styles');

    /*
     * Theme scripts - generally loaded at the bottom of the page
     */
    function jcs_enqueue_scripts() {

        // WordPress pre-installed
        wp_enqueue_script('jquery');

        // BootStrap 4.5.0
        wp_register_script('bootstrapJS', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js', array('jquery'), '4.5.0', true);
        wp_enqueue_script('bootstrapJS');

        // FluidBox
        wp_register_script('debounce', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js', array('jquery'), '1.1.0', true);
        wp_register_script('fluidboxjs', get_template_directory_uri() . '/assets/js/jquery.fluidbox.min.js', array('jquery', 'debounce'), '2.0.5', true);
        wp_enqueue_script('debounce');
        wp_enqueue_script('fluidboxjs');

        wp_register_script('rslider', get_template_directory_uri() . '/assets/royalslider/jquery.royalslider.custom.min.js', array('jquery'), '1.05',true);
        wp_enqueue_script('rslider');

        // Theme main script file
        wp_register_script('jcs-js', get_template_directory_uri() . '/assets/js/jcs-scripts.js', array('jquery'), '1.0.0', true);
        wp_enqueue_script('jcs-js');
        wp_localize_script(
            'jcs-js',
            'admin_ajax',
            array('ajax_url' => admin_url('admin-ajax.php'))
        );

    }
    add_action('wp_enqueue_scripts', 'jcs_enqueue_scripts');

    // Only Load these scripts on the front end (header.php)
    function jcs_header_scripts() {

        if ( $GLOBALS['pagenow'] != 'wp-login.php' && !is_admin() ) {
            // Needed for SVG Handling
            wp_register_script('conditionizer', get_template_directory_uri() . '/assets/js/conditionizr-4.3.0.min.js', array(), '4.3.0', false); // Conditionizr
            wp_enqueue_script('conditionizer'); // Enqueue it!
            wp_register_script('modernizr', get_template_directory_uri() . '/assets/js/modernizr-2.7.1.min.js', array('jquery'), '2.7.1', false); // Modernizr
            wp_enqueue_script('modernizr'); // Enqueue it!
        }
    }
    add_action('init', 'jcs_header_scripts');

    /*
     * Conditional Scripts
     */
    function jcs_conditional_scripts() {
        if ( is_page('contact') ) {
            // ReCaptcha
            $site_key = ( get_option('jcs_is_live') ) ? get_option('jcs_recaptcha_v3_site_key') : get_option('jcs_recaptcha_v3_site_key_staging');
            wp_register_script('reCaptcha-v3', 'https://www.google.com/recaptcha/api.js?render=' . $site_key, array(), 'v3', true);
            wp_enqueue_script('reCaptcha-v3');
            wp_add_inline_script(
                'jcs-js',
                'let f_site_key = ' . json_encode( array("value" => $site_key) ),
                'before'
            );
            wp_enqueue_script('jcs-js');
        }
        
        if ( is_page('videos') ) {
            wp_register_script('jcs-videos', get_template_directory_uri() . '/assets/js/jcs-videos.js', array('jquery'), '1.0', true);
            wp_enqueue_script('jcs-videos');
        }
    }
    add_action('wp_enqueue_scripts', 'jcs_conditional_scripts');

    /*
     * Register Theme Menus. Two provided - Main and Footer. Add as many as you want.
     */
    function jcs_menus() {
        register_nav_menus( array(
            'prog_nav' => __('Programs Menu', 'jcs-strong-home')
        ));
        register_nav_menus( array(
            'footer_menu' => __('Footer Menu', 'jcs-strong-home')
        ));
        register_nav_menus( array(
            'peril_menu' => __('Peril Menu', 'jcs-strong-home')
        ));
        register_nav_menus( array(
            'social_menu' => __('Social Menu', 'jcs-strong-home')
        ));
    }
    add_action('init', 'jcs_menus');
    
    /**
     * Add category capability to attachments
     */
    function add_categories_to_attachments() {
        register_taxonomy_for_object_type('category', 'attachment');
    }
    //add_action('init', 'add_categories_to_attachments');
    function add_tags_to_attachments() {
        register_taxonomy_for_object_type('post_tag', 'attachment');
    }
    //add_action('init', 'add_tags_to_attachments');

    /*
     * AJAX function for swapping projects on front-page
     */
    function jcs_swap_project() {
        global $post_id;
        $post_id = $_POST['id'];
        $post_type = get_post_type($post_id);
        if ($post_type == 'flash_project') {
            get_template_part('parts/content', 'projects');
        }
        exit();
    }
    add_action('wp_ajax_jcs_swap_project', 'jcs_swap_project');
    add_action('wp_ajax_nopriv_jcs_swap_project', 'jcs_swap_project');
    
    /*
     * Admin listing customizations
     */
    function define_flash_project_posts_columns($columns) : array {
        unset($columns['date']);
        unset($columns['ssid']);
        return array_merge($columns,
            [
                'show_on_front_page' => __('Show on Front Page', 'jcs-strong-home'),
                'date' => __('Date', 'jcs-strong-home'),
                'ssid' => __('ID', 'jcs-strong-home')
            ]
        );
    }
    add_filter('manage_flash_project_posts_columns', 'define_flash_project_posts_columns');
    
    function jcs_flash_projects_columns($column_key, $post_id) {
        
        //echo $column_key . "<br />";
    
        if ( $column_key == 'show_on_front_page' ) {
            $show = get_field('show_on_front_page', $post_id);
            _e(ucfirst($show), 'jcs-strong-home');
        }

    }
    add_action('manage_flash_project_posts_custom_column', 'jcs_flash_projects_columns', 10, 2);
    
    /*
     * Return an array of states for the registration form select list.  The state
     * IDs have to match those in the refStates table of the MPC shopping cart DB
     * because after validation this form is going to post to MPC.
     */
    function get_states() {
        $stateArr = array(
            array('id' => 2, 'name' => 'Alabama', 'abbr' => 'AL'),
            array('id' => 1, 'name' => 'Alaska', 'abbr' => 'AK'),
            array('id' => 4, 'name' => 'Arizona', 'abbr' => 'AZ'),
            array('id' => 3, 'name' => 'Arkansas', 'abbr' => 'AR'),
            array('id' => 5, 'name' => 'California', 'abbr' => 'CA'),
            array('id' => 6, 'name' => 'Colorado', 'abbr' => 'CO'),
            array('id' => 7, 'name' => 'Connecticut', 'abbr' => 'CT'),
            array('id' => 9, 'name' => 'Delaware', 'abbr' => 'DE'),
            array('id' => 10, 'name' => 'Florida', 'abbr' => 'FL'),
            array('id' => 11, 'name' => 'Georgia', 'abbr' => 'GA'),
            array('id' => 12, 'name' => 'Hawaii', 'abbr' => 'HI'),
            array('id' => 14, 'name' => 'Idaho', 'abbr' => 'ID'),
            array('id' => 15, 'name' => 'Illinois', 'abbr' => 'IL'),
            array('id' => 16, 'name' => 'Indiana', 'abbr' => 'IN'),
            array('id' => 13, 'name' => 'Iowa', 'abbr' => 'IA'),
            array('id' => 17, 'name' => 'Kansas', 'abbr' => 'KS'),
            array('id' => 18, 'name' => 'Kentucky', 'abbr' => 'KY'),
            array('id' => 19, 'name' => 'Louisiana', 'abbr' => 'LA'),
            array('id' => 22, 'name' => 'Maine', 'abbr' => 'ME'),
            array('id' => 21, 'name' => 'Maryland', 'abbr' => 'MD'),
            array('id' => 20, 'name' => 'Massachusetts', 'abbr' => 'MA'),
            array('id' => 23, 'name' => 'Michigan', 'abbr' => 'MI'),
            array('id' => 24, 'name' => 'Minnesota', 'abbr' => 'MN'),
            array('id' => 26, 'name' => 'Mississippi', 'abbr' => 'MS'),
            array('id' => 25, 'name' => 'Missouri', 'abbr' => 'MO'),
            array('id' => 27, 'name' => 'Montana', 'abbr' => 'MT'),
            array('id' => 30, 'name' => 'Nebraska', 'abbr' => 'NE'),
            array('id' => 34, 'name' => 'Nevada', 'abbr' => 'NV'),
            array('id' => 31, 'name' => 'New Hampshire', 'abbr' => 'NH'),
            array('id' => 32, 'name' => 'New Jersey', 'abbr' => 'NJ'),
            array('id' => 33, 'name' => 'New Mexico', 'abbr' => 'NM'),
            array('id' => 35, 'name' => 'New York', 'abbr' => 'NY'),
            array('id' => 28, 'name' => 'North Carolina', 'abbr' => 'NC'),
            array('id' => 29, 'name' => 'North Dakota', 'abbr' => 'ND'),
            array('id' => 36, 'name' => 'Ohio', 'abbr' => 'OH'),
            array('id' => 37, 'name' => 'Oklahoma', 'abbr' => 'OK'),
            array('id' => 38, 'name' => 'Oregon', 'abbr' => 'OR'),
            array('id' => 39, 'name' => 'Pennsylvania', 'abbr' => 'PA'),
            array('id' => 40, 'name' => 'Rhode Island', 'abbr' => 'RI'),
            array('id' => 41, 'name' => 'South Carolina', 'abbr' => 'SC'),
            array('id' => 42, 'name' => 'South Dakota', 'abbr' => 'SD'),
            array('id' => 43, 'name' => 'Tennessee', 'abbr' => 'TN'),
            array('id' => 44, 'name' => 'Texas', 'abbr' => 'TX'),
            array('id' => 45, 'name' => 'Utah', 'abbr' => 'UT'),
            array('id' => 47, 'name' => 'Vermont', 'abbr' => 'VT'),
            array('id' => 46, 'name' => 'Virginia', 'abbr' => 'VA'),
            array('id' => 48, 'name' => 'Washington', 'abbr' => 'WA'),
            array('id' => 8, 'name' => 'Washington DC', 'abbr' => 'DC'),
            array('id' => 50, 'name' => 'West Virginia', 'abbr' => 'WV'),
            array('id' => 49, 'name' => 'Wisconsin', 'abbr' => 'WI'),
            array('id' => 51, 'name' => 'Wyoming', 'abbr' => 'WY')
        );
        return $stateArr;
    }

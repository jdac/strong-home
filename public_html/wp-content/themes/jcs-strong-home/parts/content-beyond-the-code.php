<?php
/* 
 * Project:    strong-home
 * File:       ${NAME}
 * Created:    Mar 07, 2022 11:04 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description:
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
$args = array(
    'post_type' => 'post',
    'name' => 'beyond-the-code-design-makes-a-strong-home',
    'post_status' => 'publish',
    'numberposts' => 1
);
$main_post = new WP_Query($args);

$intro_args = array(
    'post_type' => 'post',
    'name' => 'ibhs-gold-intro',
    'post_status' => 'publish',
    'numberposts' => 1
);
$intro_post = new WP_Query($intro_args);

$gold_args = array(
    'post_type' => 'gold_standard',
    'post_status' => 'publish',
    'posts_per_page' => -1
);
$standards = new WP_Query($gold_args);
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-10 offset-md-1 strong-design">
            <?php while( $main_post->have_posts() ) : $main_post->the_post(); ?>
                <h2><?php echo get_the_title(); ?></h2>
                <p><?php echo get_the_content(); ?></p>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <div class="col-12 col-md-6 offset-md-1 house">
            <?php while( $standards->have_posts() ) : $standards->the_post(); ?>
                <a class="gs-toggle <?php echo get_field('button_class'); ?>"
                   data-target="<?php echo get_the_ID(); ?>"
                   title="<?php echo get_the_title(); ?>"
                ></a>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <div class="col-12 col-md-4 explain">
            <?php while( $intro_post->have_posts() ) : $intro_post->the_post(); ?>
                <div id="<?php echo get_the_ID(); ?>" class="gold-intro">
                    <?php echo get_the_content(); ?>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
            <?php while( $standards->have_posts() ) : $standards->the_post(); ?>
                <div id="<?php echo get_the_ID(); ?>" class="gold-standard">
                    <h3><?php echo get_the_title(); ?></h3>
                    <p><?php echo get_the_content(); ?></p>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>
</div>


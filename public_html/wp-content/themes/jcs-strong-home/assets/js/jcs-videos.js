/*
 * Project:    Strong-Homes
 * File:       jcs-videos.js
 * Created:    Mar 29, 2021 2:42:23 PM
 * Updated:
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://jdacsolutions.com
 *
 * Description:  Handles JS functions for the site videos page
 *
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

$ = jQuery.noConflict();

console.log('Videos Javascript loaded');

$(document).ready(function() {

    $("#training_intro").royalSlider({
        // options go here
        // as an example, enable keyboard arrows nav
        keyboardNavEnabled: true,
        autoScaleSlider: true,
        autoScaleSliderWidth: 4,
        autoScaleSliderHeight: 2.5,
        imageScaleMode: 'fill',
        //imageScalePadding: 0,
        autoPlay: {
            enabled: false
        },
        loop: false,
        arrowsNav: false,
        transitionType: 'fade',
        //controlNavigation: 'thumbnails',
        controlNavigation: 'none',
        thumbs: {
            spacing: 15,
            fitInViewport: false,
            autoCenter: true,
            arrowLeft: $('#fp_thumb_left'),
            arrowRight: $('#fp_thumb_right')
        },
        globalCaption: false
    });

    $(".video-tile.royalSlider").royalSlider({
        // options go here
        // as an example, enable keyboard arrows nav
        keyboardNavEnabled: true,
        autoScaleSlider: true,
        autoScaleSliderWidth: 4,
        autoScaleSliderHeight: 2.5,
        imageScaleMode: 'fill',
        //imageScalePadding: 0,
        autoPlay: {
            enabled: false
        },
        loop: false,
        arrowsNav: false,
        transitionType: 'fade',
        //controlNavigation: 'thumbnails',
        controlNavigation: 'none',
        thumbs: {
            spacing: 15,
            fitInViewport: false,
            autoCenter: true,
            arrowLeft: $('#fp_thumb_left'),
            arrowRight: $('#fp_thumb_right')
        },
        globalCaption: true
    });

});

// Toggles for accordion sections
$('.btn-box').on('click', function() {
    toggle_videos('#' + $(this).attr('id'));
});

function toggle_content(button, content_div) {
    let this_div = $(content_div);
    let btn_id = button + ' div.arrow';
    //console.log(btn_id);
    let btn = $(btn_id);
    if (this_div.hasClass('show')) {
        this_div.removeClass('show');
        btn.removeClass('open');
    } else {
        this_div.addClass('show');
        btn.addClass('open');
    }
}

function toggle_videos(button) {
    let videos_target = button + '_videos';
    console.log(videos_target);
    let btn_id = button + ' div.arrow';
    //console.log(btn_id);
    let btn = $(btn_id);
    if (btn.hasClass('open')) {
        btn.removeClass('open');
        $(videos_target).removeClass('show');
    } else {
        btn.addClass('open');
        $(videos_target).addClass('show');
    }
}

// Expand all/Close All link
$('#expander').on('click', function() {
    let link = $(this).text();
    //console.log(link);
    if ('Expand All' === link) {
        $('#meteoro_content').addClass('show');
        $('#pop_m_tk div.arrow').addClass('open');
        $('#podcasts_content').addClass('show');
        $('#pop_pc div.arrow').addClass('open');
        $('#social_content').addClass('show');
        $('#pop_s_tk div.arrow').addClass('open');
        $(this).text('Close All');
    } else {
        $('#meteoro_content').removeClass('show');
        $('#pop_m_tk div.arrow').removeClass('open');
        $('#podcasts_content').removeClass('show');
        $('#pop_pc div.arrow').removeClass('open');
        $('#social_content').removeClass('show');
        $('#pop_s_tk div.arrow').removeClass('open');
        $(this).text('Expand All');
    }
});

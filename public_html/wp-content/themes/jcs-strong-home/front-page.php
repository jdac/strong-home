<?php
/* 
 * Project:    strong-home
 * File:       front-page.php
 * Created:    Feb 11, 2022 9:50 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template for site home page.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
    get_header();
    $p_level = '';
?>

    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

    <section id="program_links">
        <?php get_template_part('parts/content', 'programs'); ?>
    </section>

    <section id="hero_content">
        <?php
        while( have_posts() ): the_post();
            get_template_part('parts/content', 'strong-home');
        endwhile;
        ?>
    </section>

    <section id="initiative">
        <?php get_template_part('parts/content', 'fp-video-slider'); ?>
    </section>

    <section id="cta_1" class="call-to-action">
        <?php $cta_slug = 'affordable-resilient-housing'; get_template_part('parts/content', 'fp-cta'); ?>
    </section>

    <section id="projects">
        <?php get_template_part('parts/content', 'projects'); ?>
    </section>

    <section id="beyond_the_code" class="blue-check-bg" style="min-height: 40rem;">
        <?php get_template_part('parts/content', 'beyond-the-code'); ?>
    </section>

    <section id="cta_2" class="call-to-action">
        <?php $cta_slug = 'train-volunteers'; get_template_part('parts/content', 'fp-cta'); ?>
    </section>

    <section id="sponsors">
        <div class="container">
            <?php get_template_part('parts/content', 'sponsors'); ?>
        </div>
    </section>

    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

<?php
get_footer();
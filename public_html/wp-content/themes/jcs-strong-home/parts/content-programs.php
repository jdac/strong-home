<?php
/* 
 * Project:    strong-home
 * File:       content-programs
 * Created:    Feb 11, 2022 10:37 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering the FLASH Program links on the site.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */

$args = array(
    'post_type' => 'flash_program',
    'posts_per_page' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => 'active',
            'value' => 'yes',
            'compare' => '='
        )
    )
);
$programs = new WP_Query($args);
$added_count = 0;
global $language;
?>
<div class="container d-none d-sm-block">
    <div class="row">
        <h1>FLASH Programs</h1>
        <div class="col-12 prog-links">
            <p>
                <?php
                while( $programs->have_posts() ) : $programs->the_post();
                    $added_count++;
                    $program = get_the_title();
                    $link = get_field('program_link');
                    ?>
                    <a href="<?php echo $link; ?>" <?php echo ($program == 'Strong Homes') ? 'class="active"' : ''; ?>><?php echo $program; ?></a>
                <?php endwhile; wp_reset_postdata(); ?>
            </p>
        </div>
    </div>
</div>
<?php
    $url_donors = get_page_by_path('donors');
    $url_videos = get_page_by_path('videos');
    $url_contact = get_page_by_path('contact');
?>
<div id="pages_banner">
    <h1>
        <a href="<?php echo get_permalink($url_donors); ?>" <?php echo (is_page('donors')) ? 'class="active"' : ''; ?>>Donors <span class="arrow-link"></span></a>
        <a href="<?php echo get_permalink($url_videos); ?>" <?php echo (is_page('videos')) ? 'class="active"' : ''; ?>>Videos <span class="arrow-link"></span></a>
        <a href="<?php echo get_permalink($url_contact); ?>" <?php echo (is_page('contact')) ? 'class="active"' : ''; ?>>Contact Us <span class="arrow-link"></span></a>
    </h1>
</div>

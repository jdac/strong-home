<?php
/* 
 * Project:    strong-home
 * File:       content-fp-video-slider.php
 * Created:    Feb 12, 2022 10:18 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering the strong homes initiative video on the front page
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
$args = array(
    'post_type' => 'flash_video',
    'posts_per_page' => 1,
    'meta_query' => array(
        array(
            'key' => 'show_on_front_page',
            'value' => 'yes',
            'compare' => '='
        )
    )
);
$videos = new WP_Query($args);
$num_videos = $videos->post_count;
?>
<div class="container">
    <div class="row fp_video_wrapper">
        <div id="fp_videos" class="royalSlider rsDefault col-12 col-md-10 offset-md-1">
            <?php while( $videos->have_posts() ) : $videos->the_post();
                $img_uri = get_the_post_thumbnail_url(get_the_ID(), 'medium_large');
                $thumb_uri = get_the_post_thumbnail_url(get_the_ID(), 'jcs-video-thumb');
                $vid_link = get_field('video_link');
                $caption = get_field('caption');
                ?>
                <div class="rsContent">
                    <img class="rsImg" src="<?php echo $img_uri; ?>" data-rsVideo="<?php echo $vid_link; ?>" />
                    <?php if ($caption != '') : ?>
                        <div class="rsCaption"><?php echo $caption; ?></div>
                    <?php endif; ?>
                    <div class="rsTmb">
                        <img src="<?php echo $thumb_uri; ?>" />
                        <div class="thumb-cap"><p><?php echo $caption; ?></p></div>
                    </div>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <div id="fp_thumb_left" style="display: none;"></div>
        <div id="fp_thumb_right" style="display: none;"></div>
    </div>
</div>
<?php
/* 
 * Project:    strong-home
 * File:       inner-donor-info.php
 * Created:    Mar 20, 2022 9:20 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for showing donor information tiles on donors page.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */

$args = array(
    'post_type' => 'donor_info',
    'post_status' => 'publish',
    'posts_per_page' => -1
);
$donor_types = new WP_Query($args);
$odd = true;

?>
<div class="container">
    <div class="row">
        <div class="di-content col-12 col-md-10 offset-md-1">
            <?php while ( $donor_types->have_posts() ): $donor_types->the_post(); ?>
                <div class="participate <?php echo ($odd) ? 'odd' : ''; ?>">
                    <?php
                    $thumb_uri = get_the_post_thumbnail_url(get_the_ID(), 'jcs-donor-tile');
                    $title = get_the_title();
                    ?>
                    <img src="<?php echo $thumb_uri; ?>" alt="<?php echo $title; ?>" />
                    <h4><?php echo $title; ?></h4>
                    <?php echo get_the_content(); ?>
                </div>
                <?php $odd = !$odd; ?>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <div class="di-contact col-12 col-md-10 offset-md-1">
            <h3>Find out how you can make a difference.</h3>
            <div class="contact-cta"><a href="/contact">Contact Us</a></div>
        </div>
    </div>
</div>

<?php
/* 
 * Project:    strong-home
 * File:       form-donor-contact.php
 * Created:    Mar 21, 2022 11:08 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering the donor contact form.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
 ?>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-8 offset-md-2 form-content">
            <form class="donor-form" method="post" id="donor_inputs">
                <input type="hidden" name="jcs_donors" id="jcs_donors" value="1" />
                <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" value="" />
                <div class="contest-rules">
                    <h3>Contact Information</h3>
                </div>
                <div class="input-fields">
                    <div class="field-row">
                        <input class="input one-two" type="text" name="first_name" id="first_name" required placeholder="First Name" />
                        <label for="first_name">First Name</label>
                        <input class="input one-two" type="text" name="last_name" id="last_name" required placeholder="Last Name" />
                        <label for="last_name">Last Name</label>
                    </div>
                </div>
                <div class="input-fields">
                    <div class="field-row">
                        <input class="input input-full" type="text" name="title" id="title" placeholder="Title" />
                        <label for="title">Title</label>
                    </div>
                </div>
                <div class="input-fields">
                    <div class="field-row">
                        <input class="input input-full" type="text" name="organization" id="organization" required placeholder="Organization" />
                        <label for="title">Organization</label>
                    </div>
                </div>
                <div class="input-fields">
                    <div class="field-row">
                        <input class="input three-five" type="email" name="email" id="email" required placeholder="Email" />
                        <label for="email">Email Address</label>
                        <input class="input two-five" type="text" name="phone" id="phone" placeholder="Phone" />
                        <label for="phone">Telephone number</label>
                    </div>
                </div>
                <div class="input-centered enter-contest">
                    <input type="submit" name="donor_submit" id="donor_submit" value="Submit" />
                </div>
            </form>
        </div>

    </div>
</div>

<?php
    /* 
     * Project:    strong-home
     * File:       inner-video-disclaimer.php
     * Created:    Apr 06, 2022 10:38 AM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description: Template part for displaying the disclaimer at the bottom of the video page.
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'name' => 'video-disclaimer',
        'posts_per_page' => 1
    );
    $d_post = new WP_Query($args);
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-10 offset-md-1 v-disclaimer">
            <?php while( $d_post->have_posts() ): $d_post->the_post(); ?>
                <p><em><?php echo get_the_content(); ?></em></p>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>
</div>

<?php
/* 
 * Project:    strong-home
 * File:       content-sponsors.php
 * Created:    Mar 10, 2022 11:52 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for displaying sponsor icons on the site front page.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
/*$args = array(
    'post_type' => 'flash_sponsor',
    'posts_per_page' => -1,
    'meta_query' => array(
        array(
            'key' => 'sponsor_class',
            'value' => 'national',
            'compare' => '='
        )
    ),
    'orderby' => 'title',
    'order' => 'ASC'
);
$sponsors = new WP_Query($args);
$count = $sponsors->post_count; */
    $count = 1;
?>
<div class="container">
    <?php if ($count > 0) : ?>
        <div class="row partners">
            <div class="col-12 col-md-10 offset-md-1">
                <h2><span>Thank you to our National Sponsors</span></h2>
            </div>
            <?php get_template_part('parts/content', 'sponsor-logos-sm'); ?>
            <?php get_template_part('parts/content', 'sponsor-logos-md'); ?>
            <?php get_template_part('parts/content', 'sponsor-logos-lg'); ?>
        </div>
    <?php endif; ?>
</div>

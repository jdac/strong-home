<?php
/* 
 * Project:    strong-home
 * File:       content-fp-cta.php
 * Created:    Feb 12, 2022 11:49 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering Calls to Action on the front page.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */

    global $cta_slug;

    $args = array(
        'post_type' => 'flash_cta',
        'posts_per_page' => 1,
        'name' => $cta_slug
    );

    $the_cta = new WP_Query($args);
?>
<div class="container">
    <div class="row">
        <?php while ( $the_cta->have_posts() ): $the_cta->the_post();
            $the_bg = get_the_post_thumbnail_url(get_the_ID());
        ?>
            <div class="col-12 col-md-10 offset-md-1 cta-wrapper" style="background-image: url(<?php echo $the_bg; ?>);">
                <div class="cta-content">
                    <h3><?php echo get_field('heading'); ?></h3>
                    <?php if (get_field('subtext')) : ?>
                        <p><?php echo get_field('subtext'); ?></p>
                    <?php endif; ?>
                </div>
                <div class="cta-action">
                    <a href="<?php echo get_field('cta_link'); ?>"><?php echo get_field('cta_label'); ?></a>
                </div>
            </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
</div>

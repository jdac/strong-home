<?php
/* 
 * Project:    strong-home
 * File:       content-resilience-actions.php
 * Created:    Feb 11, 2022 1:33 PM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering the resilience icons and actions in the front-page hero section.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
$args = array(
    'post_type' => 'res_action',
    'posts_per_page' => -1,
    'orderby' => 'menu_order'
);
$actions = new WP_Query($args);

while ( $actions->have_posts() ) : $actions->the_post();
?>
    <div class="res-action">
        <div class="icon">
            <img src="<?php echo get_field('icon'); ?>" alt="<?php echo get_field('action_text'); ?>" />
        </div>
        <div class="action-text">
            <p><?php echo get_field('action_text'); ?></p>
        </div>
    </div>
<?php endwhile; wp_reset_postdata();

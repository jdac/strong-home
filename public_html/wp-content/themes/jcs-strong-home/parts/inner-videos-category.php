<?php
    /* 
     * Project:    strong-home
     * File:       inner-videos-category.php
     * Created:    Mar 26, 2022 9:38 AM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description: Template part for rendering videos by category.
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    global $video_cat;
    
    $args = array(
        'post_type' => 'training_video',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_query' => array(
            'relationship' => 'AND',
            array(
                'key' => 'video_category',
                'value' => $video_cat,
                'compare' => 'LIKE'
            ),
            array(
                'key' => 'show_at_top',
                'value' => 'no',
                'compare' => '='
            )
        )
    );
    
    $videos = new WP_Query($args);
    
    while ( $videos->have_posts() ): $videos->the_post();
        $vid_link = get_field('video_link');
        $img_uri = get_the_post_thumbnail_url(get_the_ID(), 'jcs-training-thumb');
?>
        <div class="sh-video">
            <div class="video-tile-header">
                <h4><?php echo get_the_title(); ?></h4>
            </div>
            <div class="video-tile royalSlider rsDefault">
                <div class="rsContent">
                    <img class="rsImg" src="<?php echo $img_uri; ?>" data-rsVideo="<?php echo $vid_link; ?>" />
                </div>
            </div>
            <?php if (get_field('video_length')) :  ?>
                <h5><?php echo get_field('video_length'); ?></h5>
            <?php endif; ?>
        </div>
<?php
    endwhile; wp_reset_postdata();
    
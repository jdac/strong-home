<?php
/* 
 * Project:    strong-home
 * File:       header.php
 * Created:    Feb 11, 2022 9:47 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Site-wide header template
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- make site iOS compatible -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php echo get_bloginfo(); ?>">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri() . '/favicon.png'; ?>">

    <!-- make site Android compatible -->
    <meta name="theme-color" content="#a61206">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Site Name">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() . '/favicon.png'; ?>" >

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','<?php echo get_option("jcs_gtag_id"); ?>')</script>
    <!-- End Google Tag Manager -->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo get_option("jcs_gtag_id"); ?>"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="site-header" >
    <nav class="navbar navbar-expand-md main-navigation navbar-light">
        <a class="navbar-brand" href="https://flash.org">
            <img src="<?php echo get_template_directory_uri() . '/assets/images/flash-logo.png'; ?>" class="img-responsive" alt="Tornado Strong" />
        </a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#prog_menu">
            <span class="sr-text">Toggle Navigation</span>
            <?php // This creates the hamburger ?>
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-right d-sm-none t-strong">
            <?php
            // Again, all the classes below are provided by BootStrap
            wp_nav_menu( array(
                'theme_location' => 'prog_nav',
                'container_id' => 'prog_menu',
                'container_class' => 'collapse navbar-collapse',
                'menu_class' => 'nav navbar-nav navbar-right'
            ));
            ?>
        </div> <!-- .navbar-right -->
    </nav>

</header>
<?php
    /* 
     * Project:    strong-home
     * File:       ${NAME}
     * Created:    Mar 29, 2022 4:31 PM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description:
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    $sm_args = array(
        'post_type' => 'flash_sponsor',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'sponsor_class',
                'value' => 'national',
                'compare' => '='
            )
        ),
        'orderby' => 'title',
        'order' => 'ASC'
    );
    $sponsors_sm = new WP_Query($sm_args);
    $count = $sponsors_sm->post_count;
?>
<div class="logo-spreader d-md-none">
    <?php while( $sponsors_sm->have_posts() ) :
        $sponsors_sm->the_post();
        $the_link = get_field('link');
        $img_uri = get_field('logo');
        ?>
        <?php if ($img_uri) : ?>
            <div><a href="<?php echo $the_link; ?>"><img src="<?php echo $img_uri; ?>" alt="<?php echo get_the_title(); ?>" /></a></div>
        <?php endif; ?>
    <?php endwhile; wp_reset_postdata(); ?>
</div>


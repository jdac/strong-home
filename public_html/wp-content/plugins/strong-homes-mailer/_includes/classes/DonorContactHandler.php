<?php
/* 
 * Project:    strong-home
 * Class:      DonorContactHandler
 * Created:    Mar 22, 2022 12:03 PM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description:  The DonorContactHandler class is responsible for email creation and handling for donor contact
 * form submissions.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */


namespace strong_homes_mailer;


class DonorContactHandler {

    public function submit_form(): int {
        if (count($_POST) > 0) {
            // send mail from here
            $msg = $this->get_HTML_req_msg();
            // multiple recipients
            //$to  = 'aidan@example.com' . ', '; // note the comma
            // DEV ONLY - once fully tested and ready to deploy replace with commented line below
            //$to = EMAIL_LEADS_NOTIFICATION;
            $to = $this->get_recipient();
            // subject
            $subject = get_bloginfo('description') . ' Donor Contact Form Notification';
            // Additional headers
            $headers = [];
            $headers[] = 'From: ' . get_option('jcs_site_email_from_name') . ' <' . get_option('jcs_site_email_from') . '>';
            $headers[] = 'Bcc:webadmin@jdacsolutions.com';
            $headers[] = 'Reply-To: ' . sanitize_email($_POST['requester-email']);
            // To send HTML mail, the Content-type header must be set
            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';
            // Mail it
            if (wp_mail($to, $subject, $msg, $headers)) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return 0;
        }
    }

    public function get_recipient(): string {
        return (get_option('jcs_is_live')) ? get_option('jcs_contact_email_to') : get_option('jcs_contact_email_to_staging');
    }

    private function get_HTML_req_msg(): string {

        $msg_body = '
                <!DOCTYPE html>
                <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title> ' . get_bloginfo('description') . ' Donor Contact Form Request</title>
                    </head>
                    <body>
                        <table width="600" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                <!--[if (gte mso 9)|(IE)]>
                                <table width="600" align="left" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                <![endif]-->
                                    <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td width="25%" align="left">
                                                <img src="' . get_template_directory_uri() . '/assets/images/flash-logo.png" alt="FLASH Strong Homes" width="250" height="127" />
                                            </td>
                                            <td align="right"><h3 style="color:#004A80;">' . get_bloginfo('description') . ' Donor Contact Request</h3></td>
                                        </tr>
                                        <tr><td colspan="2">&nbsp;</td></tr>
                                    </table>
                                <!--[if (gte mso 9)|(IE)]>
                                        </td>
                                    </tr>
                                </table>
                                <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <!--[if (gte mso 9)|(IE)]>
                                    <table width="600" align="left" cellpadding="20px 5px 20px 5px" cellspacing="0" border="0" style="background-color: #E3E4E5;">
                                        <tr>
                                            <td>
                                <![endif]-->
                                    <table width="100%" align="left" cellpadding="20px 5px 20px 5px" cellspacing="0" border="0" style="background-color: #ffffff; padding: 2px;">
                                        <tr style="padding-bottom: 10px;">
                                            <td width="25%" align="left"><strong>From: </strong></td>
                                            <td width="75%" align="left">' . sanitize_text_field($_POST['first_name']) . ' ' . sanitize_text_field($_POST['last_name']) .  ' <span><</span>' . sanitize_email($_POST['email']) . '<span>></span></td>
                                        </tr>
                                        <tr style="padding-bottom: 10px;">
                                            <td width="25%" align="left"><strong>Title: </strong></td>
                                            <td width="75%" align="left">' . sanitize_text_field($_POST['title']) . '</td>
                                        </tr>
                                        <tr style="padding-bottom: 10px;">
                                            <td width="25%" align="left"><strong>Organization: </strong></td>
                                            <td width="75%" align="left">' . sanitize_text_field($_POST['organization']) . '</td>
                                        </tr>
                                        <tr style="padding-bottom: 10px;">
                                            <td width="25%" align="left"><strong>Phone: </strong></td>
                                            <td width="75%" align="left">' . sanitize_text_field($_POST['phone']) . '</td>
                                        </tr>
                                    </table>
                                <!--[if (gte mso 9)|(IE)]>
                                        </td>
                                    </tr>
                                </table>
                                <![endif]-->               
                                </td>
                            </tr>
                        </table>
                    </body>
                </html>';
        return $msg_body;
    }

}

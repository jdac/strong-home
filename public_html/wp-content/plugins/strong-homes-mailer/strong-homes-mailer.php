<?php
/*
 * Plugin Name: Strong Homes Mailer
 * Plugin URI:  https://bitbucket.org/jdac/strong-homes
 * Description: Handles mailer functions for form submissions
 * Version:     1.0
 * Created:    Mar 22, 2022 11:50 AM
 * Author:     DriveJCS
 * Author URI: https://drivejcs.com
 *
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Copyright: 2022 FLASH.  All rights reserved.
 *
 * This program is free software; you can distribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY;  without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Se the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

require( 'vendor/autoload.php');

// Call function when plugin is activated
register_activation_hook(__FILE__, 'strong_homes_mailer_install');

function strong_homes_mailer_install() {
    // Set up default option values
    $jcs_options_arr = array(
        'default_lan' => 'en-US'
    );
    update_option('strong_homes_mailer_options', $jcs_options_arr);
    flush_rewrite_rules();
}

function submit_contact_request():int {
    $mailer = new \strong_homes_mailer\DonorContactHandler();
    return $mailer->submit_form();
}



<?php
    /*
     * Project:    FLASH WP NDRC
     * File:       content-separator.php
     * Created:    Oct 09, 2021 10:16
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     *
     * Description: Renders the separator bars below the header and above the footer.
     *
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     *
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
?>
    <div class="container">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-2"></div>
            <div class="col-2"></div>
            <div class="col-2"></div>
            <div class="col-2"></div>
            <div class="col-2"></div>
        </div>
    </div>

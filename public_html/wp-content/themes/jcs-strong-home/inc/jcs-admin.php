<?php
    /* 
     * Project:    strong-home
     * File:       jcs-admin.php
     * Created:    Mar 25, 2022 9:59 AM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description: Defines theme modifications to WP Admin behaviors.
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    global $pagenow;
    
    /*
     * Donors CPT Admin Listing Mods
     */
    // Add custom columns to Admin edit.php listing
    function jcs_define_donor_posts_columns($columns) : array {
        unset($columns['date']);
        unset($columns['ssid']);
        return array_merge($columns,
            [
                'donor_state' => __('State', 'jcs-strong-homes'),
                'date' => __('Date', 'jcs-strong-homes'),
                'ssid' => __('ID', 'jcs-strong-homes')
            ]
        );
    }

    // Make the donor state column sortable
    function jcs_donor_sortable_columns( $columns ) {
        $columns['donor_state'] = 'donor_state';
        return $columns;
    }

    // Populate the donor columns
    function jcs_donor_columns($column_key, $post_id) {
        //echo $column_key . "<br />";
        if ( $column_key == 'title' ) {
            $title = get_field('donor_name', $post_id);
            _e($title, 'jcs-strong-homes');
        }
        if ( $column_key == 'donor_state' ) {
            $d_state = get_post_meta($post_id, 'donor_state', true);
            _e($d_state, 'jcs-strong-homes');
        }
    }
    
    // Save the donor name as the post title
    function jcs_set_donor_title( $post_id ) {
        if (get_post_meta( $post_id, 'donor_name', true )) {
            $title = get_post_meta( $post_id, 'donor_name', true );
            $data = array(
                'ID'         => $post_id,
                'post_title' => $title,
                'post_name'  => sanitize_title( $title ),
            );
            wp_update_post( $data );
        }
    }
    add_action( 'acf/save_post', 'jcs_set_donor_title', 20 );
    
    // Set up the sort for the donor_state
    function jcs_sort_custom_donor_state_column_query( $query ) {
        $orderby = $query->get( 'orderby' );
        if ( 'donor_state' == $orderby ) {
            $meta_query = array(
                'relation' => 'OR',
                array(
                    'key' => 'donor_state',
                    'compare' => 'NOT EXISTS', // see note above
                ),
                array(
                    'key' => 'donor_state',
                ),
            );
            $query->set( 'meta_query', $meta_query );
            $query->set( 'orderby', 'meta_value' );
        }
    }
    
    /*
     * Training Video CPT Admin Listing Mods
     */
    // Add custom columns to WP Admin edit.php listing
    function jcs_define_training_video_posts_columns($columns) : array {
        unset($columns['date']);
        unset($columns['ssid']);
        return array_merge($columns,
            [
                'video_category' => __('Category', 'jcs-strong-homes'),
                'date' => __('Date', 'jcs-strong-homes'),
                'ssid' => __('ID', 'jcs-strong-homes')
            ]
        );
    }
    
    // Make the training video category column sortable
    function jcs_training_video_sortable_columns( $columns ) {
        $columns['video_category'] = 'video_category';
        return $columns;
    }
    
    // Populate the columns
    function jcs_training_video_columns($column_key, $post_id) {
        //echo $column_key . "<br />";
        if ( $column_key == 'video_category' ) {
            $v_cat = get_field('video_category', $post_id);
            _e($v_cat['label'], 'jcs-strong-homes');
        }
    }
    
    // Set up the sort for the video_category
    function jcs_sort_custom_video_category_column_query( $query ) {
        $orderby = $query->get( 'orderby' );
        if ( 'video_category' == $orderby ) {
            $meta_query = array(
                'relation' => 'OR',
                array(
                    'key' => 'video_category',
                    'compare' => 'NOT EXISTS', // see note above
                ),
                array(
                    'key' => 'video_category',
                ),
            );
            $query->set( 'meta_query', $meta_query );
            $query->set( 'orderby', 'meta_value' );
        }
    }
    
    // Apply Filters
    if ( 'donor' == $_GET['post_type'] ) {
        // manage columns
        add_filter('manage_donor_posts_columns', 'jcs_define_donor_posts_columns');
        // make columns sortable
        add_filter( 'manage_edit-donor_sortable_columns', 'jcs_donor_sortable_columns');
        // populate column cells
        add_action('manage_donor_posts_custom_column', 'jcs_donor_columns', 10, 2);
        // set query to sort
        add_action( 'pre_get_posts', 'jcs_sort_custom_donor_state_column_query' );
    }
    
    if ( 'training_video' == $_GET['post_type'] ) {
        // manage columns
        add_filter('manage_training_video_posts_columns', 'jcs_define_training_video_posts_columns');
        // make columns sortable
        add_filter( 'manage_edit-training_video_sortable_columns', 'jcs_training_video_sortable_columns');
        // populate column cells
        add_action('manage_training_video_posts_custom_column', 'jcs_training_video_columns', 10, 2);
        // set query to sort
        add_action( 'pre_get_posts', 'jcs_sort_custom_video_category_column_query' );
    }
    
<?php
    /* 
     * Project:    strong-home
     * File:       inner-video-categories.php
     * Created:    Mar 25, 2022 2:06 PM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description: Renders the accordion boxes for the training videos section on the videos page.
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    $video_cats = array(
        'fortified' => 'FORTIFIED Standards',
        'foundation' => 'Foundation',
        'wall' => 'Walls',
        'roof' => 'Roof',
        'porch' => 'Porch',
        'opening' => 'Opening Protection'
    );
    global $video_cat;
?>
<div class="container">
    <div class="row">
        <?php foreach( $video_cats as $key => $value ): $video_cat = $key; ?>
            <div class="col-12 col-md-10 offset-md-1 video-dropdown">
                <div class="video-icon"></div>
                <div class="video-cat"><h3><?php echo $value; ?></h3></div>
                <div class="cat-expander">
                    <div id="<?php echo $key; ?>" class="btn-box">
                        <div class="arrow"></div>
                    </div>
                </div>
            </div>
            <div id="<?php echo $key; ?>_videos" class="col-12 col-md-10 offset-md-1 vids-wrapper">
                <?php get_template_part('parts/inner', 'videos-category'); ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>

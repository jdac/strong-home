<?php
    /* 
     * Project:    strong-home
     * File:       inner-state-donors.php
     * Created:    Mar 23, 2022 10:31 AM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description: Template part for listing state project sponsors in the donors page thank you section.
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    global $state;
    $args = array(
        'post_type' => 'donor',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'donor_state',
                'value' => $state,
                'compare' => '='
            )
        ),
        'orderby' => 'title',
        'order' => 'ASC'
    );
    $state_donors = new WP_Query($args);
    $donors_found = $state_donors->found_posts;
    
?>
<?php if ( $donors_found > 0 ) : ?>
<div class="row">
    <div class="col-8 col-md-4 offset-2 offset-md-4 state-label">
        <h3><?php echo $state; ?></h3>
    </div>
</div>
<div class="row">
    <?php while( $state_donors->have_posts() ): $state_donors->the_post(); ?>
        <div class="col-12 col-md-4 state-donor">
            <?php
                $donor_link = get_field('donor_url');
                if ($donor_link) :
            ?>
                    <h4><a href="<?php echo $donor_link; ?>" target="_blank"><?php echo get_field('donor_name'); ?></a></h4>
            <?php else: ?>
                    <h4><?php echo get_field('donor_name'); ?></h4>
            <?php endif; ?>
        </div>
    <?php endwhile; wp_reset_postdata(); ?>
</div>
<?php endif;

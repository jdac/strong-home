<?php
    /* 
     * Project:    strong-home
     * File:       videos.php
     * Template Name: Videos
     * Created:    Mar 23, 2022 12:40 PM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description: Top-level template for the videos page.
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    get_header();
?>
    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>
    
    <section id="program_links">
        <?php get_template_part('parts/content', 'programs'); ?>
    </section>
    
    <section id="hero_content">
        <?php
            while( have_posts() ): the_post();
                get_template_part('parts/content', 'strong-home');
            endwhile;
        ?>
    </section>
    
    <section id="videos_intro">
        <?php get_template_part('parts/inner', 'video-slider'); ?>
    </section>
    
    <section id="video_wraps">
        <?php get_template_part('parts/inner', 'video-categories'); ?>
    </section>
    
    <section id="disclaimer">
        <?php get_template_part('parts/inner', 'video-disclaimer'); ?>
    </section>
    
    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

<?php
    get_footer();
